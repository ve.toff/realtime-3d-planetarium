using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SolarSystemUI : MonoBehaviour
{
    public static SolarSystemUI Instance;
    private SolarSystem solarSystem;

    [Header("Time Panel")]
    public GameObject dateText;
    public GameObject timeText;
    public GameObject sliderSpeed;
    public GameObject speedLabel;

    [Header("Planet Info Panel")]
    public GameObject planetInfoPanel;
    public GameObject planetNameObject;
    public GameObject planetDescriptionObject;

    [Header("PlanetUI")]
    public GameObject planetsUI;


    void Start()
    {
        solarSystem = SolarSystem.Instance;
    }


    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            TimeReset();

    }


    public void SetPlanetInfo(string name, string descrpiption)
    {
        planetNameObject.GetComponent<TextMeshProUGUI>().text = "";
        planetDescriptionObject.GetComponent<TextMeshProUGUI>().text = "";

        if (name != null && name.Length > 0)
            planetNameObject.GetComponent<TextMeshProUGUI>().text = name;

        if (descrpiption != null && descrpiption.Length > 0)
            planetDescriptionObject.GetComponent<TextMeshProUGUI>().text = descrpiption;
    }

    public void HidePlanetInfoPanel()
    {
        planetInfoPanel.GetComponent<Animator>().SetBool("panel_info_show", false);
    }

    public void ShowPlanetInfoPanel()
    {
        planetInfoPanel.GetComponent<Animator>().SetBool("panel_info_show", true);
    }

    public bool isPanelInfoShow()
    {
        return planetInfoPanel.GetComponent<Animator>().GetBool("panel_info_show");
    }

    public void ClearPlanetInfo()
    {
        planetNameObject.GetComponent<TextMeshProUGUI>().text = "";
        planetDescriptionObject.GetComponent<TextMeshProUGUI>().text = "";
    }

    public void SetDate(string date)
    {
        dateText.GetComponent<TextMeshProUGUI>().text = date;
    }

    public void SetTime(string time)
    {
        timeText.GetComponent<TextMeshProUGUI>().text = time;
    }

    public void SetSpeedLabel()
    {
        float value = sliderSpeed.GetComponent<Slider>().value;

        speedLabel.GetComponent<TextMeshProUGUI>().text = string.Format("{0}x", value * 10);
    }

    public void TimeReset()
    {
        solarSystem.RealTimePositions();
        sliderSpeed.GetComponent<Slider>().value = 0f;
    }

    public void SliderSpeed()
    {
        float value = sliderSpeed.GetComponent<Slider>().value;
        solarSystem.ChangeSpeed(value * 10);
    }

    public void PlanetsUISetVisible(bool visible)
    {
        foreach (Transform child in planetsUI.transform)
        {
            Color color = child.GetComponent<Image>().color;

            if (visible)
                color.a = 1;
            else
                color.a = 0;
            child.GetComponent<Image>().color = color;
        }
    }

}
