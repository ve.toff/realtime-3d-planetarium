using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetUI : MonoBehaviour
{

    private Camera mainCamera;
    public Transform lookAtPlanet;
    public GameObject planetOrbit;

    private bool objIsCulled;
    public bool onScreen = true;


    void Start()
    {
        mainCamera = Camera.main;
    }


    void LateUpdate()
    {

        Vector3 pos = mainCamera.WorldToScreenPoint(lookAtPlanet.position);

        objIsCulled = isObjectLodCulled(lookAtPlanet.gameObject);

        if (pos.z >= 0)
        {
            if (objIsCulled)
            {
                this.GetComponent<Image>().enabled = true;

                pos = new Vector3(pos.x, pos.y, 0);
                if (transform.position != pos)
                    transform.position = pos;
            }
            else if (!objIsCulled)
                this.GetComponent<Image>().enabled = false;
        }
    }

    private bool isObjectLodCulled(GameObject obj)
    {

        LODGroup lodGroup = obj.GetComponent<LODGroup>();
        if (lodGroup != null)
        {

            Transform lodTransform = lodGroup.transform;
            foreach (Transform child in lodTransform)
            {
                Renderer renderer = child.GetComponent<Renderer> ();
                if (renderer != null && renderer.isVisible)
                {
                    return false;

                }
                return true;

            }
        }
        return false;
    }

}
