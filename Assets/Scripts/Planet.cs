using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{

    [Header("GameObjects")]
    public GameObject planet;
    public GameObject planetCenter;
    public GameObject planetOrbit;
    public GameObject planetPosition;

    [Header("Movements")]
    private bool resetMovement = false;
    private float speedMultiplier = 0.0f;

    private bool rotation = false;
    public float rotationSpeed;


    private bool orbiting = false;
    public float orbitingSpeed;


    private PlanetData planetData;
    private LineRenderer orbit;
    private DateTime newRotationDate;

    void Awake()
    {
        orbit = planetOrbit.GetComponent<LineRenderer>();
        planetData = gameObject.GetComponent<PlanetData>();
    }

    public void InitOrbiting(DateTime currentDate)
    {
        float lon = GetRotation(currentDate);
        planetPosition.transform.eulerAngles = new Vector3(0, -lon, 0);

        int nb = FindClosestPoint();
        planetPosition.transform.eulerAngles = new Vector3(0, 0, 0);

        StartCoroutine(Movements(nb));
    }

    public void InitEarthRotation(DateTime currentDate)
    {
        planet.transform.eulerAngles = new Vector3(0, 0, 0);

        float r;
        r = (6.283185307f * (currentDate.Hour * 3600 + currentDate.Minute * 60 + currentDate.Second) / 43200) * 180 / Mathf.PI;

        planet.transform.Rotate(0, -r / 2.0f, 0, Space.World);

        newRotationDate = currentDate;
    }

    public int FindClosestPoint()
    {
        float distance = float.MaxValue;
        float distanceTemp;
        int nb = 0;
        for (int i = 0; i < orbit.positionCount - 1; i++)
        {
            distanceTemp = Vector3.Distance(
                               planetPosition.transform.GetChild(0).transform.position,
                               orbit.transform.TransformPoint(orbit.GetPosition(i)));

            if (distanceTemp < distance)
            {
                distance = distanceTemp;
                nb = i;
            }
        }
        return nb;
    }

    IEnumerator Movements(int nb)
    {
        resetMovement = true;
        yield return new WaitUntil(() => !orbiting);
        yield return new WaitUntil(() => !rotation);
        resetMovement = false;

        InitEarthRotation(newRotationDate);

        StartCoroutine(Rotation());
        StartCoroutine(Orbiting(nb));
    }

    IEnumerator Rotation()
    {
        rotation = true;
        if (gameObject.name.Equals("Earth"))
        {
            DateTime rotationDate = newRotationDate;
            while (!resetMovement)
            {
                yield return new WaitUntil(() => speedMultiplier != 0 || resetMovement);

                if (speedMultiplier != 0)
                    yield return new WaitForSeconds(0.0001f);

                rotationDate = SolarSystem.Instance.GetCurrentTime();
                SolarSystem.Instance.UpdateTime(rotationDate);

                planet.transform.eulerAngles = new Vector3(0, 0, 0);
                float r;
                r = (6.283185307f * (rotationDate.Hour * 3600 + rotationDate.Minute * 60 + rotationDate.Second) / 43200) * 180 / Mathf.PI;

                planet.transform.Rotate(0, -r / 2.0f, 0, Space.Self);

                yield return null;
            }
        }
        else
        {
            planet.transform.eulerAngles = new Vector3(0, 0, 0);
            while (!resetMovement)
            {
                planet.transform.Rotate(0, rotationSpeed * speedMultiplier * Time.deltaTime, 0);
                yield return null;
            }
        }
        rotation = false;
    }

    IEnumerator Orbiting(int nb)
    {
        orbiting = true;
        int count;

        if (nb < orbit.positionCount - 1)
            count = nb + 1;
        else
            count = 0;

        Transform orbitTransform = orbit.transform;

        transform.localPosition = orbit.transform.TransformPoint(orbit.GetPosition(nb));

        while (!resetMovement)
        {
            Vector3 nextPosition = orbitTransform.TransformPoint(orbit.GetPosition(count));

            while (transform.localPosition != nextPosition && !resetMovement)
            {
                yield return new WaitUntil(() => speedMultiplier != 0 || resetMovement);

                transform.localPosition = Vector3.MoveTowards(transform.localPosition, nextPosition, Mathf.Abs(speedMultiplier) * orbitingSpeed * planetData.orbitingSpeedMultiplier * Time.deltaTime);
                yield return null;
            }

            if (gameObject.name.Equals("Earth") && !resetMovement)
            {
                newRotationDate = newRotationDate.AddDays((speedMultiplier > 0) ? 1f : -1f);
                SolarSystem.Instance.UpdateDate(newRotationDate);
            }

            if (speedMultiplier < 0)
            {
                if (count > 0)
                    count--;
                else
                    count = orbit.positionCount - 1;
            }
            else
            {
                if (count < orbit.positionCount - 1)
                    count++;
                else
                    count = 0;
            }


        }
        orbiting = false;
    }


    public float GetRotation(DateTime currentDate)
    {

        float E0, E1, x, y, dist, v, xeclip, yeclip, zeclip, lon, lat, r, d;
        d = 367 * currentDate.Year - 7 * (currentDate.Year + (currentDate.Month + 9) / 12) / 4 + 275 * currentDate.Month / 9 + currentDate.Day - 730530;
        d += currentDate.Hour / 24.0f + currentDate.Minute / 60.0f;

        float N = planetData.GetN(d);
        float i = planetData.GetI(d);
        float w = planetData.GetW(d);
        float a = planetData.GetA(d);
        float e = planetData.GetE(d);
        float M = planetData.GetM(d);


        E0 = M + (180 / Mathf.PI) * e * Mathf.Sin(M * Mathf.PI / 180) * (1 + e * Mathf.Cos(M * Mathf.PI / 180));
        E1 = E0 - (E0 - (180 / Mathf.PI) * e * Mathf.Sin(E0 * Mathf.PI / 180) - M) / (1 - e * Mathf.Cos(E0 * Mathf.PI / 180));


        while (Mathf.Abs(E0 - E1) > 0.005)
        {
            E0 = E1;
            E1 = E0 - (E0 - (180 / Mathf.PI) * e * Mathf.Sin(E0 * Mathf.PI / 180) - M) / (1 - e * Mathf.Cos(E0 * Mathf.PI / 180));
        }

        x = a * (Mathf.Cos(E1 * Mathf.PI / 180) - e);
        y = a * Mathf.Sqrt(1 - e * e) * Mathf.Sin(E1 * Mathf.PI / 180);


        dist = Mathf.Sqrt(x * x + y * y);
        v = Mathf.Atan2(y, x) * 180 / Mathf.PI;


        xeclip = dist * (Mathf.Cos(N * Mathf.PI / 180) *
                         Mathf.Cos((v + w) * Mathf.PI / 180) - Mathf.Sin(N * Mathf.PI / 180) *
                         Mathf.Sin((v + w) * Mathf.PI / 180) * Mathf.Cos(i * Mathf.PI / 180));

        yeclip = dist * (Mathf.Sin(N * Mathf.PI / 180) *
                         Mathf.Cos((v + w) * Mathf.PI / 180) + Mathf.Cos(N * Mathf.PI / 180) *
                         Mathf.Sin((v + w) * Mathf.PI / 180) * Mathf.Cos(i * Mathf.PI / 180));

        zeclip = dist * Mathf.Sin((v + w) * Mathf.PI / 180) * Mathf.Sin(i * Mathf.PI / 180);


        lon = Mathf.Atan2(yeclip, xeclip) * 180 / Mathf.PI;
        lat = Mathf.Atan2(zeclip, Mathf.Sqrt(xeclip * xeclip + yeclip * yeclip)) * 180 / Mathf.PI;
        r = Mathf.Sqrt(xeclip * xeclip + yeclip * yeclip + zeclip * zeclip);

        if (lon < 0)
            lon += 360;

        return lon;
    }

    public void UpdateSpeedMultiplier(float speed)
    {
        speedMultiplier = speed;
    }
}
