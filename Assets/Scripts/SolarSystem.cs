using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarSystem : MonoBehaviour
{
    public static SolarSystem Instance;

    private SolarSystemUI solarSystemUI;

    private List<Planet> listOfPlanets;

    public GameObject solarSystemCenter;
    public GameObject cameraPosition;

    [Header("Planets")]
    public Planet mercuryPlanet;
    public Planet venusPlanet;
    public Planet earthPlanet;
    public Planet marsPlanet;
    public Planet jupiterPlanet;
    public Planet saturnPlanet;
    public Planet uranusPlanet;
    public Planet neptunePlanet;

    [Header("Satellites")]
    public Moon moonSatellite;


    public DateTime currentDate;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        solarSystemUI = SolarSystemUI.Instance;

        listOfPlanets = new List<Planet>();
        listOfPlanets.Add(mercuryPlanet);
        listOfPlanets.Add(venusPlanet);
        listOfPlanets.Add(earthPlanet);
        listOfPlanets.Add(marsPlanet);
        listOfPlanets.Add(jupiterPlanet);
        listOfPlanets.Add(saturnPlanet);
        listOfPlanets.Add(uranusPlanet);
        listOfPlanets.Add(neptunePlanet);

        RealTimePositions();
    }

    public void RealTimePositions()
    {

        // Использовать UtcNow или просто Now?
        currentDate = DateTime.UtcNow;

        solarSystemUI.SetDate(currentDate.ToString("dd.MM.yyyy"));
        solarSystemUI.SetTime(currentDate.ToString("HH:mm"));

        earthPlanet.InitEarthRotation(currentDate);

        moonSatellite.UpdateSpeedMultiplier(0.0f);
        moonSatellite.InitOrbiting(currentDate);

        foreach (Planet planet in listOfPlanets)
        {
            planet.UpdateSpeedMultiplier(0.0f);
            planet.InitOrbiting(currentDate);
        }

    }

    public void AddMinutes(float minutes)
    {
        currentDate = currentDate.AddMinutes(minutes);
        solarSystemUI.SetTime(currentDate.ToString("HH:mm"));
    }

    public void UpdateDate(DateTime date)
    {
        solarSystemUI.SetDate(date.ToString("dd.MM.yyyy"));
    }

    public void UpdateTime(DateTime date)
    {
        solarSystemUI.SetTime(date.ToString("HH:mm"));
    }

    public DateTime GetCurrentTime()
    {
        return DateTime.UtcNow;
    }

    public void ChangeSpeed(float speed)
    {
        foreach (Planet planet in listOfPlanets)
        {
            planet.UpdateSpeedMultiplier(speed);
        }
        moonSatellite.UpdateSpeedMultiplier(speed);
    }
}
