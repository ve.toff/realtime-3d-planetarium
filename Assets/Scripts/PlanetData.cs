using System.IO;
using UnityEngine;

public class PlanetData : MonoBehaviour
{
    [Header("Название планеты")]
    public string planetName = "";
    [Space]

    [Header("Множитель орбитальной скорости")]
    public float orbitingSpeedMultiplier = 0f;
    [Space]

    [Header("Долгота восходящего узла")]
    public float N;
    public float N_date;
    [Header("Наклон к эклиптике (плоскоти орбиты Земли)")]
    public float i;
    public float i_date;
    [Header("Аргумент перигелия")]
    public float w;
    public float w_date;
    [Header("Большая полуось (среднее расстояние от Солнца)")]
    public float a;
    public float a_date;
    [Header("Эксцентриситет (0 - круг, 0-1 - элипс, 1 - парабола)")]
    public float e;
    public float e_date;
    [Header("Средняя погрешность (0 в перигелии; равномерно увеличивается со времением)")]
    public float M;
    public float M_date;

    [Header("Файл описанием объекта из Resources/PlanetsWiki")]
    public TextAsset planetDescriptionFile;

    public string planetDescription;


    private void Awake()
    {
        planetDescription = readPlanetDescriptionFile();
    }


    private string readPlanetDescriptionFile()
    {
        if (planetDescriptionFile != null)
        {
            string text = planetDescriptionFile.text;
            return text;
        }
        else
        {
            return "";
        }

    }

    public float GetN(float d)
    {
        return N + N_date * d;
    }

    public float GetI(float d)
    {
        return i + i_date * d;
    }

    public float GetW(float d)
    {
        return w + w_date * d;
    }

    public float GetA(float d)
    {
        return a + a_date * d;
    }

    public float GetE(float d)
    {
        return e + e_date * d;
    }

    public float GetM(float d)
    {
        return M + M_date * d;
    }
}
