using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class CameraController : MonoBehaviour
{
    public static CameraController Instance;

    private SolarSystemUI solarSystemUI;
    private enum Mode { isIdle, isRotating, isZooming, isPanning };
    private Mode mode = Mode.isIdle;

    private float xDeg = 0.0f;
    private float yDeg = 0.0f;
    private bool isLocked = false;
    private bool isLocking = false;
    private Transform targetRotation;
    private Vector3 offset;
    private Vector3 off = Vector3.zero;
    private Camera mainCamera;
    private Vector3 desiredPosition;
    private Vector3 CamPlanePoint;
    private Vector3 vectorPoint;
    private AudioSource audioSource;


    public GameObject lockedObject
    {
        get;
        private set;
    }

    public GameObject centerOfSolarSystem;
    public GameObject cameraTopDownPosition;
    public GameObject directionalLightObject;
    public AudioClip cameraWhooshEffect;
    public float xSpeed = 10.0f;
    public float ySpeed = 10.0f;
    public float yMinLimit = -80.0f;
    public float yMaxLimit = 80.0f;
    public float zoomRate = 10.0f;
    public float panSpeed = 0.3f;
    public float minZoomDistance = 20.0f;
    public float maxZoomDistance = 2000.0f;
    public float rotationDampening = 5.0f;
    public int panThres = 5;
    public bool panMode = false;



    void Awake()
    {

        Instance = this;

        solarSystemUI = SolarSystemUI.Instance;
        targetRotation = new GameObject("CameraTargetRotation").transform;
        mainCamera = gameObject.GetComponent<Camera>();
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    void Start()
    {
        targetRotation.position = Vector3.zero;
    }

    void LateUpdate()
    {
        Ray ray = mainCamera.ScreenPointToRay (Input.mousePosition);
        float wheel = Input.GetAxis ("Mouse ScrollWheel");

        RaycastHit hit;

        if (isLocked)
        {
            offset = lockedObject.transform.position - off;
            off = lockedObject.transform.position;

            if (Input.GetMouseButton(1) == false)
            {
                mode = Mode.isIdle;

                float magnitude = (targetRotation.position - transform.position).magnitude;
                transform.position = targetRotation.position - (transform.rotation * Vector3.forward * magnitude) + offset;
                targetRotation.position = targetRotation.position + offset;
            }

        }


        if (Input.GetMouseButton (1))
            mode = Mode.isRotating;

        else if (Input.GetMouseButtonUp (1))
        {
            yDeg = transform.rotation.eulerAngles.x;
            if (yDeg > 180)
                yDeg -= 360;

            xDeg = transform.rotation.eulerAngles.y;
            mode = Mode.isIdle;

        }

        else if (MouseXBoarder () != 0 || MouseYBoarder () != 0)
            mode = Mode.isPanning;

        else if (wheel != 0)
        {
            if (!IsPointerOverUIObject())
            {
                mode = Mode.isZooming;
            }
        }


        else if (Input.GetKey(KeyCode.Escape))
        {
            if (lockedObject != null)
                UnlockObject ();
        }

        else if (Input.GetMouseButtonDown(0) && Physics.Raycast (ray, out hit, float.MaxValue) == true)
        {
            if (!IsPointerOverUIObject())
            {
                GameObject l_object = hit.collider.transform.parent.parent.gameObject;
                if (lockedObject != l_object)
                    LockObject (l_object);
            }

        }


        switch (mode)
        {
        case Mode.isIdle:
            break;

        case Mode.isRotating:

            xDeg += Input.GetAxis ("Mouse X") * xSpeed;
            yDeg -= Input.GetAxis ("Mouse Y") * ySpeed;
            yDeg = ClampAngle (yDeg, yMinLimit, yMaxLimit, 5);

            transform.rotation = Quaternion.Lerp (
                                     transform.rotation, Quaternion.Euler (yDeg, xDeg, 0), Time.deltaTime * rotationDampening / Time.timeScale);
            targetRotation.rotation = transform.rotation;

            float magnitude = (targetRotation.position - transform.position).magnitude;
            transform.position = targetRotation.position - (transform.rotation * Vector3.forward * magnitude) + offset;
            targetRotation.position = targetRotation.position + offset;
            break;

        case Mode.isZooming:

            float s0 = LinePlaneIntersect (
                           transform.forward, transform.position, Vector3.up, Vector2.zero, ref CamPlanePoint);

            targetRotation.position = transform.forward * s0 + transform.position;

            float lineToPlaneLength = LinePlaneIntersect (
                                          ray.direction, transform.position, Vector3.up, Vector2.zero, ref vectorPoint);

            float zoomDistance = Vector3.Distance(centerOfSolarSystem.transform.position, transform.position);


            if (wheel > 0)
            {
                if (zoomDistance > -minZoomDistance)
                    desiredPosition = ((vectorPoint - transform.position) / 2 + transform.position);
            }
            else if (wheel < 0)
            {
                if (zoomDistance < maxZoomDistance)
                    desiredPosition = (-(targetRotation.position - transform.position) / 2 + transform.position);
            }

            transform.position = Vector3.Lerp (
                                     transform.position, desiredPosition, zoomRate * Time.deltaTime / Time.timeScale);

            if (transform.position == desiredPosition)
                mode = Mode.isIdle;

            break;

        case Mode.isPanning:

            if (panMode == true)
            {
                float panNorm = transform.position.y;
                if ((Input.mousePosition.x - Screen.width + panThres) > 0)
                {
                    targetRotation.Translate (Vector3.right * -panSpeed * Time.deltaTime * panNorm);
                    transform.Translate (Vector3.right * -panSpeed * Time.deltaTime * panNorm);
                }
                else if ((Input.mousePosition.x - panThres) < 0)
                {
                    targetRotation.Translate (Vector3.right * panSpeed * Time.deltaTime * panNorm);
                    transform.Translate (Vector3.right * panSpeed * Time.deltaTime * panNorm);
                }
                if ((Input.mousePosition.y - Screen.height + panThres) > 0)
                {
                    vectorPoint.Set (transform.forward.x, 0, transform.forward.z);
                    targetRotation.Translate (vectorPoint.normalized * -panSpeed * Time.deltaTime * panNorm, Space.World);
                    transform.Translate (vectorPoint.normalized * -panSpeed * Time.deltaTime * panNorm, Space.World);
                }
                if ((Input.mousePosition.y - panThres) < 0)
                {
                    vectorPoint.Set (transform.forward.x, 0, transform.forward.z);
                    targetRotation.Translate (vectorPoint.normalized * panSpeed * Time.deltaTime * panNorm, Space.World);
                    transform.Translate (vectorPoint.normalized * panSpeed * Time.deltaTime * panNorm, Space.World);
                }
            }
            break;

        default:
            break;
        }
    }


    private int MouseXBoarder()
    {
        if ((Input.mousePosition.x - Screen.width + panThres) > 0)
            return 1;
        else if ((Input.mousePosition.x - panThres) < 0)
            return -1;
        else
            return 0;
    }


    private int MouseYBoarder()
    {
        if ((Input.mousePosition.y - Screen.height + panThres) > 0)
            return 1;
        else if ((Input.mousePosition.y - panThres) < 0)
            return -1;
        else
            return 0;
    }

    private static float ClampAngle (float angle, float minOuter, float maxOuter, float inner)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;

        angle = Mathf.Clamp (angle, minOuter, maxOuter);

        if (angle < inner && angle > 0)
            angle -= 2 * inner;
        else if (angle > -inner && angle < 0)
            angle += 2 * inner;

        return angle;
    }

    private float LinePlaneIntersect (Vector3 u, Vector3 P0, Vector3 N, Vector3 D, ref Vector3 point)
    {
        float s = Vector3.Dot (N, (D - P0)) / Vector3.Dot (N, u);
        point = P0 + s * u;
        return s;
    }

    public void LockObject (GameObject objectToLock)
    {
        mode = Mode.isIdle;

        isLocked = true;

        if (lockedObject != objectToLock && !isLocking)
        {
            lockedObject = objectToLock;

            off = objectToLock.transform.position;

            Vector3 camPosition = lockedObject.transform.Find("PlanetCenter/PlanetCameraPosition").position;
            
            if (directionalLightObject == null)
                Debug.Log(this.ToString());
            
            directionalLightObject.transform.LookAt(lockedObject.transform.localPosition);

            targetRotation.position = lockedObject.transform.position;
            targetRotation.rotation = directionalLightObject.transform.rotation;


            // Если плавное приближение камеры не очень, то использовать следующее
            //transform.position = camPosition;
            // Иначе вот
            StartCoroutine(moveCameraToObject(camPosition, targetRotation.rotation, 1f));

            audioSource.PlayOneShot(cameraWhooshEffect);

            solarSystemUI.SetPlanetInfo(
                lockedObject.GetComponent<PlanetData>().planetName,
                lockedObject.GetComponent<PlanetData>().planetDescription);

            if (!solarSystemUI.isPanelInfoShow())
                solarSystemUI.ShowPlanetInfoPanel();

            solarSystemUI.PlanetsUISetVisible(false);

        }
    }


    IEnumerator moveCameraToObject(Vector3 target, Quaternion lookAt, float duration)
    {
        isLocking = true;
        float time = 0;
        Vector3 startPosition = transform.position;
        Quaternion startRotation = transform.rotation;

        while(time < duration)
        {
            transform.position = Vector3.Lerp(startPosition, target, time / duration);
            transform.rotation = Quaternion.Slerp(startRotation, lookAt, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = target;
        isLocking = false;

    }

    private void UnlockObject ()
    {
        isLocked = false;
        lockedObject = null;
        offset = Vector3.zero;
        solarSystemUI.ClearPlanetInfo();
        if (solarSystemUI.isPanelInfoShow())
            solarSystemUI.HidePlanetInfoPanel();

        solarSystemUI.PlanetsUISetVisible(true);

    }


    private bool IsPointerOverUIObject()
    {
        /* Предотвращает нажатия через слой UI */

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public bool isCameraControllerLocked()
    {
        if (isLocked && lockedObject != null)
            return true;
        else
            return false;
    }
}
