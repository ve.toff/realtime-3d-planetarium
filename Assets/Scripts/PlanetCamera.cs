using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetCamera : MonoBehaviour
{

    private Camera mainCamera;
    private Vector3 dragOrigin;
    private Vector3 move;

    public float maxRotation = 90;
    public float dragSpeed = 2f;
    public bool isActive = false;

    void Start()
    {
        mainCamera = Camera.main;
    }


    void Update()
    {
        if (!isActive)
            return;

        if (Input.GetMouseButtonDown (1))
        {
            dragOrigin = Input.mousePosition;
            move = transform.eulerAngles;
            return;
        }

        if (Input.GetMouseButton (1))
        {
            Vector3 pos = mainCamera.ScreenToViewportPoint (Input.mousePosition - dragOrigin);

            if (ClampEulerAngle (move.x + (pos.y * dragSpeed)) >= -maxRotation &&
                    ClampEulerAngle (move.x + (pos.y * dragSpeed)) <= maxRotation)
            {
                move += new Vector3 (pos.y * dragSpeed, pos.x * dragSpeed, 0);
            }
            else
            {
                move += new Vector3 (0, pos.x * dragSpeed, 0);
            }

            transform.eulerAngles = move;
        }
    }

    private static float ClampEulerAngle(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < -180)
        {
            result += 360f;
        }
        return result;
    }
}
